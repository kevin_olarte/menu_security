import React from 'react'

import { ExampleComponent } from 'menu_security'
import 'menu_security/dist/index.css'

const App = () => {
  return <ExampleComponent text="Create React Library Example 😄" />
}

export default App
